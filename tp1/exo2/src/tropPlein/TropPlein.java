package tropPlein;

import javax.naming.LimitExceededException;
import java.time.LocalDate;

public class TropPlein {
    private final int id;
    private static int nbTropPlein = 0;
    private final int capacite;
    private int quantite;

    public TropPlein(int capacite) {

        if (capacite < 1 )
            throw new IllegalArgumentException("Impossible de creer un trop plein qui a une capacite inferieur a 1");
        else{
            nbTropPlein += 1;
            this.id = nbTropPlein;
            this.capacite = capacite;
            this.quantite = 0;
        }

    }

    public void ajoutLiquide(int quantite) throws LimitExceededException {
        if (quantite > capacite){
            this.quantite = capacite;
            throw new LimitExceededException("La cuve et le trop plein sont rempli au maximum !");
        }
        else if (quantite > capacite / 2){
            this.quantite = quantite;
            throw new LimitExceededException("Attention le trop plein est a moitié plein, penser a effectuer une vidange");
        }

    }

    public int getCapacite() {
        return capacite;
    }

    public int getQuantite() {
        return quantite;
    }
}
