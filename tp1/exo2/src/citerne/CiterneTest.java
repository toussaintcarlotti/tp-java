package citerne;

import citerneSecurisee.CiterneSecurisee;
import liquide.Liquide;
import org.junit.jupiter.api.Test;
import tropPlein.TropPlein;

import javax.naming.LimitExceededException;
import java.io.ByteArrayInputStream;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class CiterneTest {


    Citerne c1 = new Citerne(11000, Liquide.EAU);

    CiterneTest() throws CiterneException {

    }
    @Test
    void constructCiterne() throws Exception{

        CiterneException eMax = assertThrows(CiterneException.class, () ->
                new Citerne(30000, Liquide.EAU));
        assertEquals("La capacité d'une cuve ne peut pas exeder 20 000 metre cube", eMax.getMessage());

        CiterneException eMin = assertThrows(CiterneException.class, () ->
                new Citerne(0, Liquide.EAU));
        assertEquals("Le volume de la citerne doit être superieur a 0", eMin.getMessage());

    }

    @Test
    void testToString() {
        assertEquals(c1.toString(), "Citerne n°" + c1.getId() + ", EAU, capacité : 11000 m3, mise en service : " +
                LocalDate.now() +
                ", volume occupé : 0");
    }

    @Test
    void nettoyage() {
        assertFalse(c1.estPropre());
        assertNotNull(c1.getTypeLiquide());
        c1.nettoyage();
        assertTrue(c1.estPropre());
        assertNull(c1.getTypeLiquide());

    }

    @Test
    void plusAncienne(){
        CiterneException e = assertThrows(CiterneException.class, () ->{
            Citerne c2 = new Citerne(1000, Liquide.EAU);
            Citerne.plusAncienne(c1, c2);
        });
        assertEquals(e.getMessage(), "Ces deux citernes ont été créer a la meme date");


    }

    @Test
    void ajouterLiquide() throws CiterneException, IllegalArgumentException, LimitExceededException {
        // test ajout metre cube
        c1.ajouterLiquide(200);
        assertEquals(c1.getQuantite(), 200);

        // test ajout pourcentage
        c1.ajouterLiquide(0.2);
        assertEquals(c1.getQuantite(), 200 + 2200);


        // entrer de l'utilisateur
        ByteArrayInputStream in = new ByteArrayInputStream("2".getBytes());
        System.setIn(in);
        // test ajout liquide lorsque l'utilisateur choisit par metre cube
        c1.ajouterLiquide(1);
        assertEquals(c1.getQuantite(), 200 + 2200 + 1);

        c1.nettoyage();
        c1.setLiquide(Liquide.EAU);
        // entrer de l'utilisateur
        in = new ByteArrayInputStream("1".getBytes());
        System.setIn(in);
        // test ajout liquide lorsque l'utilisateur choisit par pourcentage
        c1.ajouterLiquide(1);
        assertEquals(c1.getQuantite(), c1.getCapacite());
        c1.enleverLiquide(1000);

        // test de l'exception trop plein
        LimitExceededException e = assertThrows(LimitExceededException.class, () ->
                c1.ajouterLiquide(2000));
        assertEquals("La capacité maximal de la citerne a été dépasser", e.getMessage());
        assertEquals(c1.getQuantite(), c1.getCapacite());

        // test de l'exception valeur negative
        IllegalArgumentException eValN = assertThrows(IllegalArgumentException.class, () ->
                c1.ajouterLiquide(-5));
        assertEquals("Impossible d'ajouter une quantité negative: si vous voulez enlever du" +
                " liquide vous pouvez utiliser la méthode 'enleverLiquide()'", eValN.getMessage());

        // test de l'exception si aucun liquide n'est specifier
        c1.nettoyage();
        CiterneException eLiquidNotSpec = assertThrows(CiterneException.class, () ->
                c1.ajouterLiquide(40));
        assertEquals("Impossible d'ajouter du liquide sans specifier le type au préalable:" +
                " vous pouvez le faire avec la methode: 'setLiquide()'", eLiquidNotSpec.getMessage());

    }

    @Test
    void enleverLiquide() {
        // tres ressemblant a ajouterLiquide()
    }

    @Test
    void compareA() throws CiterneException, LimitExceededException {
        // capacite c1 superieur a c2
        Citerne c2 = new Citerne(1000, Liquide.VIN);
        assertEquals(c1.compareA(c2), 1);

        // capacite c1 inferieur a c2
        c2 = new Citerne(15000, Liquide.VIN);
        assertEquals(c1.compareA(c2), -1);

        // capacite égale et quantite c1 superieur a c2
        c2 = new Citerne(11000, Liquide.VIN);
        c1.ajouterLiquide(400);
        assertEquals(c1.compareA(c2), 1);

        // capacite égale et quantite c1 inferieur a c2
        c2.ajouterLiquide(1000);
        assertEquals(c1.compareA(c2), -1);

        // capacite égale et quantite égale
        c1.ajouterLiquide(600);
        assertEquals(c1.compareA(c2), 0);

        // exception objet null passer en parametre
        NullPointerException eNull = assertThrows(NullPointerException.class, () ->
                c1.compareA(null));
        assertEquals("Impossible de comparer un objet null", eNull.getMessage());

        // exception objet de type different passer en parametre
        NullPointerException e = assertThrows(NullPointerException.class, () ->
                c1.compareA(15));
        assertEquals("Impossible de comparer un objet qui n'est pas instance de la classe Citerne", e.getMessage());

    }

    @Test
    void setLiquide() throws CiterneException {
        // exception meme liquide
        CiterneException eSameLiquide = assertThrows(CiterneException.class, () ->
                c1.setLiquide(Liquide.EAU));
        assertEquals("Le liquide de cette citerne correspond deja a un liquide de type : " + c1.getTypeLiquide(), eSameLiquide.getMessage());


        // exception citerne sale
        CiterneException eNotEmpty = assertThrows(CiterneException.class, () ->
                c1.setLiquide(Liquide.HUILE));
        assertEquals("Impossible de changer de liquide sans avoir effectuer " +
                "une opération de nettoyage complète (nettoyage) au préalable", eNotEmpty.getMessage());


        c1.nettoyage();
        c1.setLiquide(Liquide.VIN);
        assertEquals(c1.getTypeLiquide(), Liquide.VIN);
    }

    @Test
    void estPropre() {
    }

    @Test
    void getCapacite(){
        assertEquals(c1.getCapacite(), 11000);
    }

    @Test
    void getTypeLiquide() {
        assertEquals(c1.getTypeLiquide(), Liquide.EAU);
    }

    @Test
    void getQuantite() throws CiterneException, LimitExceededException {
        assertEquals(c1.getQuantite(), 0);
        c1.ajouterLiquide(200);
        assertEquals(c1.getQuantite(), 200);

    }

    @Test
    void getDate() {
        assertEquals(c1.getDate(), LocalDate.now());
    }

    @Test
    void getNbCuve() {
    }

    @Test
    void getId() {
    }

    @Test
    void testEquals() throws CiterneException {
        Citerne c2 = new Citerne(11000, Liquide.EAU);
        assertEquals(c1, c2);
    }

    @Test
    void citerneSecuriseeConstruct() throws CiterneException, LimitExceededException {
        IllegalArgumentException eTp = assertThrows(IllegalArgumentException.class, () ->
                new TropPlein(0));
        assertEquals("Impossible de creer un trop plein qui a une capacite inferieur a 1", eTp.getMessage());

        TropPlein tropPlein = new TropPlein(6000);

        LimitExceededException eCs = assertThrows(LimitExceededException.class, () ->
                new CiterneSecurisee(10000, Liquide.EAU, tropPlein));
        assertEquals("Veuillez enregistrer un trop plein inferieur a la moitier de la capacite maximale de la cuve", eCs.getMessage());

        CiterneSecurisee citerneSecurisee = new CiterneSecurisee(15000, Liquide.VIN, tropPlein);
        assertEquals(citerneSecurisee.getCapacite(), 15000);
    }

}