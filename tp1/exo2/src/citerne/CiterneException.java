package citerne;

public class CiterneException extends Exception {
    public CiterneException(String message){
        super(message);
    }

    public CiterneException(){
        this("Le volume de la citerne doit être superieur a 0");
    }
}
