package citerne;

import compare.EstComparable;
import liquide.Liquide;

import javax.naming.LimitExceededException;
import java.time.LocalDate;
import java.util.Scanner;

public class Citerne implements EstComparable {
    private final int id;
    private static int nbCuve = 0;
    private final int capacite;
    private int quantite;
    private Liquide liquide;
    private boolean propre;
    private final LocalDate date;


    public Citerne(int capacite, Liquide liquide) throws CiterneException {

        //lever d'exception

        if (capacite <= 0)
            throw new CiterneException();
        else if (capacite > 20000)
            throw new CiterneException("La capacité d'une cuve ne peut pas exeder 20 000 metre cube");
        else {
            nbCuve+=1;
            id = nbCuve;
            this.capacite = capacite;
            this.liquide = liquide;
            propre = false;
            quantite = 0;
            date = LocalDate.now();
        }



    }


    public void nettoyage(){
        quantite = 0;
        propre = true;
        liquide = null;
    }


    public static Citerne plusAncienne(Citerne c1, Citerne c2) throws CiterneException {
        Citerne result = null;

        if (c1.date.isBefore(c2.date)){
            result = c1;
        }else if (c1.date.isAfter(c2.date)){
            result = c2;
        }else
            throw new CiterneException("Ces deux citernes ont été créer a la meme date");


        return result;
    }



    /**
     * GETTER
     */

    public int getCapacite() {
        return capacite;
    }

    public Liquide getTypeLiquide() {
        return liquide;
    }

    public int getQuantite() {
        return quantite;
    }

    public LocalDate getDate() {
        return date;
    }

    public static int getNbCuve() {
        return nbCuve;
    }

    public boolean estPropre() {
        return propre;
    }

    public int getId() {
        return id;
    }


    /**
     * SETTER
     */



    private void ajoutPourcent(double p) throws LimitExceededException {
        if (quantite +(capacite * p) <= capacite)
            quantite += (p * capacite);
        else {
            quantite = capacite;
            limitExceptMaxCuve();
        }


    }

    private void ajoutMcube(double mc) throws LimitExceededException {
        if (quantite + mc <= capacite){
            quantite += mc;
        }else {
            quantite = capacite;
            limitExceptMaxCuve();
        }
    }

    protected void limitExceptMaxCuve() throws LimitExceededException {
        throw new LimitExceededException("La capacité maximal de la citerne a été dépasser");
    }


    public void ajouterLiquide(double qte) throws CiterneException, IllegalArgumentException, LimitExceededException {
        if (!propre){
            if (qte > 0 && qte < 1){
                ajoutPourcent(qte);
            }else if (qte == 1){
                Scanner scanner = new Scanner(System.in);
                System.out.println("Pourcentage ou metre cube?\n1)pourcentage   2)metre cube ");
                int reponse = scanner.nextInt();
                if (reponse == 1){
                    ajoutPourcent(qte);
                }else if (reponse == 2){
                    ajoutMcube(qte);
                }else
                    throw new IllegalArgumentException("Vous devez choisir entre 1 ou 2");
            }else if (qte > 1){
                ajoutMcube(qte);
            }else
                throw new IllegalArgumentException("Impossible d'ajouter une quantité negative: si vous voulez enlever du" +
                        " liquide vous pouvez utiliser la méthode 'enleverLiquide()'");
        }else
            throw new CiterneException("Impossible d'ajouter du liquide sans specifier le type au préalable:" +
                    " vous pouvez le faire avec la methode: 'setLiquide()'");
    }

    private void enleverPourcent(double p) throws LimitExceededException {
        if (quantite - (capacite * p) > 0)
            quantite -= (p * capacite);
        else {
            quantite = 0;
            throw new LimitExceededException("La citerne ne peut pas avoir une quantite inferieur a 0");
        }


    }

    private void enleverMcube(double mc) throws LimitExceededException {

        if (quantite - mc > 0){
            quantite -= mc;
        }else {
            quantite = 0;
            throw new LimitExceededException("La citerne ne peut pas avoir une quantite inferieur a 0");
        }


    }

    public void enleverLiquide(double qte) throws LimitExceededException, IllegalArgumentException, CiterneException {
        if (this.capacite > 0) {
            if (qte > 0 && qte < 1) {
                enleverPourcent(qte);
            } else if (qte == 1) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Pourcentage ou metre cube?\n1)pourcentage   2)metre cube ");
                int reponse = scanner.nextInt();
                if (reponse == 1) {
                    enleverPourcent(qte);
                } else if (reponse == 2) {
                    enleverMcube(qte);
                } else
                    throw new IllegalArgumentException("Vous devez choisir entre 1 ou 2");
            } else if (qte > 1){
                enleverMcube(qte);
            }else
                throw new CiterneException("Veuillez entrer une valeur positive, elle sera retirer de la cuve");


        }
    }

    public void setLiquide(Liquide liquide) throws CiterneException {

        if (this.liquide == liquide)
            throw new CiterneException("Le liquide de cette citerne correspond deja a un liquide de type : " + liquide);
        else if (propre){
            this.liquide = liquide;
            propre = false;
        }

        else
            throw new CiterneException("Impossible de changer de liquide sans avoir effectuer " +
                    "une opération de nettoyage complète (nettoyage) au préalable");

    }




    @Override
    public int compareA(Object o) {
        try{
            int result;
            Citerne citerne = (Citerne) o;

            if (this.capacite > citerne.capacite)
                result = 1;
            else if (this.capacite < citerne.capacite)
                result = -1;
            else {
                result = Integer.compare(this.quantite, citerne.quantite);
            }
            return result;
        } catch (NullPointerException e){
            throw new NullPointerException("Impossible de comparer un objet null");
        } catch (ClassCastException e){
            throw new NullPointerException("Impossible de comparer un objet qui n'est pas instance de la classe Citerne");
        }
    }

    @Override
    public String toString() {
        return "Citerne n°" + id +
                ", " + liquide +
                ", capacité : " + capacite +
                " m3, mise en service : " + date +
                ", volume occupé : " + quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Citerne citerne = (Citerne) o;
        return capacite == citerne.capacite && quantite == citerne.quantite && liquide == citerne.liquide && date.equals(citerne.date);
    }

}
