package citerneSecurisee;

import citerne.Citerne;
import citerne.CiterneException;
import liquide.Liquide;
import tropPlein.TropPlein;

import javax.naming.LimitExceededException;

public class CiterneSecurisee extends Citerne {
    private int capaciteTotal;
    private TropPlein tropPlein;
    public CiterneSecurisee(int capacite, Liquide liquide, TropPlein tropPlein) throws CiterneException, LimitExceededException {
        super(capacite, liquide);
        if ((tropPlein.getCapacite() < capacite / 2)){
            this.tropPlein = tropPlein;
        }else
            throw new LimitExceededException("Veuillez enregistrer un trop plein inferieur a la moitier de la capacite maximale de la cuve");

        this.capaciteTotal = tropPlein.getCapacite() + capacite;
    }


    @Override
    protected void limitExceptMaxCuve() throws LimitExceededException {
        throw new LimitExceededException("Attention la cuve principale est pleine, le trop plein va etre utilisé");
    }

    @Override
    public void ajouterLiquide(double qte) throws CiterneException, IllegalArgumentException, LimitExceededException {
        super.ajouterLiquide(qte);
        if (this.capaciteTotal < this.getQuantite() + this.tropPlein.getQuantite()){
            this.tropPlein.ajoutLiquide((int) (qte - this.getCapacite()));
        }
    }
}
