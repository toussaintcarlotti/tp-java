package liquide;

public enum Liquide {
    EAU(10),
    VIN(15),
    HUILE(9);

    private final int temperature;

    Liquide(int temperature) {
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }
}
