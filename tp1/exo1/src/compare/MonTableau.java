package compare;

import java.util.Arrays;

public class MonTableau implements EstComparable{
    private int[] tab;

    public MonTableau(int[] tab){

        this.tab = tab;
    }

    @Override
    public int compareA(Object o) {
        int somme1 = 0;
        int somme2 = 0;
        int result = 0;
        try{
            for (int j : this.tab) {
                somme1 += j;
            }
            for (int i : ((MonTableau) o).tab){
                somme2 += i;
            }
            if (somme1 > somme2)
                result = 1;
            else if (somme1 < somme2)
                result = -1;
            return result;
        } catch (NullPointerException e){
            throw new NullPointerException("Impossible de comparer un objet null");
        } catch (ClassCastException e){
            throw new NullPointerException("Impossible de comparer un objet qui n'est pas instance de la classe MonTableau");
        }

    }

}
